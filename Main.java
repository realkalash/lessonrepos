public class Main {

    public static void main(String[] args) {
        int a, b, c, answer;
        a = 1;
        b = 2;
        c = 3;
        if (a > b) {
            if (b > c) {
                answer = (a * a) + (b * b);

                System.out.println("Два наибольших - a и b. Результат: " + answer);
            } else {
                answer = (a * a) + (c * c);

                System.out.println("Два наибольших - a и c. Результат: " + answer);
            }
        } else if (c > b) {
            if (b > a) {
                answer = (b * b) + (c * c);

                System.out.println("Два наибольших - b и c. Результат: " + answer);
            }
        } else {
            System.out.println("Одно или все числа равны");
        }

    }
    //
}
